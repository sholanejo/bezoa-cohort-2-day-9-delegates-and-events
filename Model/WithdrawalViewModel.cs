﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_7.Model
{
   public class WithdrawalViewModel: EventArgs
    {
        public string Account { get; set; }
        public int Amount { get; set; }
    }
}
